# This makefile for a Kaleidoscope sketch pulls in all the targets
# required to build the example

#" /home/bjc/src/Kaleidoscope/.arduino/data/packages/keyboardio/tools/dfu-util/0.10.0-arduino1/dfu-util" --device 0x3496:0x0005 -D /tmp/kaleidoscope-bjc/output/3606415822-Model100.ino/Model100.ino.bin -R

# arduino-cli compile --fqbn keyboardio:gd32:keyboardio_model_100 --show-properties Model100.ino

_ARDUINO_CLI_COMPILE_CUSTOM_FLAGS=--build-property "runtime.tools.xpack-arm-none-eabi-gcc-9.3.1-1.3.path=/usr" --build-property "runtime.tools.dfu-util.path=/usr/sbin"
ARDUINO_DIRECTORIES_USER=$(HOME)/Arduino/
# ARDUINO_DIRECTORIES_DATA=
KALEIDOSCOPE_DIR=$(HOME)/Arduino/libraries/Kaleidoscope
#KALEIDOSCOPE_DIR=/tmp/Kaleidoscope


ifneq ($(KALEIDOSCOPE_DIR),)
search_path += $(KALEIDOSCOPE_DIR)
endif

ifneq ($(ARDUINO_DIRECTORIES_USER),)
search_path += $(ARDUINO_DIRECTORIES_USER)/hardware/keyboardio/avr/libraries/Kaleidoscope
endif

ifeq ($(shell uname -s),Darwin)
search_path += $(HOME)/Documents/Arduino/hardware/keyboardio/avr/libraries/Kaleidoscope
else
search_path += $(HOME)/Arduino/hardware/keyboardio/avr/libraries/Kaleidoscope
endif

sketch_makefile := etc/makefiles/sketch.mk

$(foreach candidate, $(search_path), $(if $(wildcard $(candidate)/$(sketch_makefile)), $(eval ks_dir ?= $(candidate))))

ifneq ($(ks_dir),)

$(info Using Kaleidoscope from $(ks_dir))

export KALEIDOSCOPE_DIR := $(ks_dir)
include $(ks_dir)/$(sketch_makefile)

else

$(info I can't find your Kaleidoscope installation.)
$(info )
$(info I tried looking in:)
$(info )
$(foreach candidate, $(search_path), $(info $(candidate)))
$(info )
$(info The easiest way to fix this is to set the 'KALEIDOSCOPE_DIR' environment)
$(info variable to the location of your Kaleidoscope directory.)

endif

gdb: ${ELF_FILE_PATH}
	arm-none-eabi-gdb -x gdbinit.container ${ELF_FILE_PATH}

cp-elf: ${ELF_FILE_PATH}
	cp ${ELF_FILE_PATH} .

cp-bin: ${BIN_FILE_PATH}
	cp ${BIN_FILE_PATH} .

null-target:
	$(info You should never see this message)
	@:
